@------------------------------------------------------------------------------- 
@ ECE 372 Design Project 1 - Button-Driven Talker (Part One)
@ Martin Rodriguez 2019
@ Driver to interface with RC8660 speech synthesizer. Utilizes button press from
@ GPIO1_30 and UART2 to communicate on an interrupt basis. 
@ Uses modified startup_ARMCA8.s to hook interrupt vector.
@ Upon button press, talker will deliver message once per press.
@-------------------------------------------------------------------------------

.text
.global _start
.global INT_DIRECTOR
_start:
@ Set up stacks for supervisor mode and IRQ mode
    LDR R13, =STACK1         @ Point to base of STACK for SVC mode
    ADD R13, R13, #0x1000    @ Point to top of STACK
    CPS #0x12                @ Switch to IRQ mode
    LDR R13, =STACK2         @ Point to IRQ stack
    ADD R13, R13, #0x1000    @ Point to top of STACK
    CPS #0x13                @ Back to SVC mode
	NOP
	
@ Set up GPIO1 to detect falling edge of button signal from GPIO1_30
@ Turn on GPIO1 CLK
    MOV R0, #0x02            @ Value to enable clock for GPIO module
    LDR R1, =0x44E000AC      @ Address of CM_PER_GPIO1_CLKCTRL register
	STR R0, [R1]             @ Write #0x02 to register
    NOP
    
@ Detect falling edge on GPIO1_30 and enable to assert POINTRPEND1
    LDR R0, =0x4804C000      @ Base address for GPIO1 registers
    ADD R1, R0, #0x14C       @ GPIO1_FALLINGDETECT register (Add #0x14C to R0, store in R1)
    MOV R2, #0x40000000      @ Load value for bit 30 (GPIO1_30)
    LDR R3, [R1]             @ Read GPIO1_FALLINGDETECT register
    ORR R3, R3, R2           @ Modify (set bit 30)
    STR R3, [R1]             @ Write back
    ADD R1, R0, #0x34        @ Address of GPIO1_IRQSTATUS_SET_0 register
    STR R2, [R1]             @ Enable GPIO1_30 request on POINTRPEND1
    LDR R1,=0x4804C02C       @ Address of GPIO1_IRQSTATUS_0
    STR R2,[R1]		         @ Write to GPIO1_IRQSTATUS_0
    NOP
    
@ Initialize INTC for GPIO1_30 interrupt signal
    LDR R1, =0x48200000      @ Base address for INTC
    MOV R2, #0x2             @ Value to reset INTC
    STR R2, [R1, #0x10]      @ Write to INT Config register
    MOV R2, #0x04            @ Value to unmask INTC INT 98, GPIOINT1A
    STR R2, [R1, #0xE8]      @ Write to INTC_MIR_CLEAR3 register
    NOP
    
@ Initialize INTC for UART2 interrupt signal
    MOV R2, #0x400          @ Unmask INTC INT 74, UART2 interrupt
    STR R2, [R1, #0xC8]     @ Write to INTC_MIR_CLEAR2 register
    NOP

@ Map UART2 signals to pins on BBB
    LDR R1, =0x44E10000     @ Base address for control module
                            @ lcd_data8 offset = 0x8C0
                            @ lcd_data9 offset = 0x8C4
                            @ spi0_d0 offset = 0x954
                            @ spi0_sclk offset = 0x950
                            @ Write a 1 in bit 5 for each input
                            @ UART2_CTSN goes to P8 pin 37 (PROC U1)
                            @ UART2_RTSN goes to P8 pin 38 (PROC U2)
                            @ UART2_TXD goes to P9 pin 21 (PROC B17)
                            @ UART2_RXD goes to P9 pin 22 (PROC A17)
@ Map PROC U1 to UART2_CTSN (input)
    MOV R2, #0x26          	@ Value to switch lcd_data8 to UART2_CTSN (Mode 6 input)
    STR R2, [R1, #0x8C0]	@ Write to control module register lcd_data8
@ Map PROC U2 to UART2_RTSN (output)
    MOV R2, #0x6         	@ Value to switch lcd_data9 to UART2_RTSN (Mode 6 output)
    STR R2, [R1, #0x8C4]	@ Write to control module register lcd_data9          
@ Map PROC B17 to UART2_TXD (output)
    MOV R2, #0x1          	@ Value to switch spi0_d0 to UART2_TXD (Mode 1 output)
    STR R2, [R1, #0x954]	@ Write to control module register spi0_d0        
@ Map PROC A17 to UART2_RXD (input)    
    MOV R2, #0x21        	@ Value to switch spi0_sclk to UART_RXD (Mode 1 input)
    STR R2, [R1, #0x950]	@ Write to control module register spi0_sclk       
	NOP
        
@ Turn on UART2 clock
    MOV R2, #0x02           @ Value to enable clock for UART2
    LDR R1, =0x44E00070     @ Address of CM_PER_UART2_CLKCTRL      
    STR R2, [R1]            @ Write 0x02 to register
    NOP    
    
@ Initialize UART2 for desired Config
	LDR R0,=0x48024000      @ Load base address for UART2 registers
	MOV R1, #0x02			@ Value to software reset UART2
	STR R1,[R0,#0x54]		@ Write to UART2 SYSC register for software reset (clear UART2 registers)
	@ Poll SYSS register to see if reset is finished (bit = 1 > reset complete)
 POLL_RESET:	
	LDR R1,=0x4802A058		@ Load address for UART2 SYSS register
    TST R1, #0x1            @ Checking if bit 0 = 1
    BNE POLL_RESET          @ If reset not complete, go back
	NOP  
    MOV R1, #0x83           @ Value to switch to Config mode A
    STR R1, [R0, #0x0C]     @ Write to Line Control Register
    
    @ Clear and disable FIFO (should be done when Baud clk is off)
    MOV R1, #0x00           @ Value to disable FIFO
    STR R1, [R0, #0x08]	    @ Write to FIFO control register       
    NOP
       	
   	@ Set Baud rate at 38.4 Kbps with a 16x divisor
    MOV R1, #0x00           @ Value to select Baud rate in DLH
    STR R1, [R0, #0x04]     @ Write to Divisor Latch High
    MOV R1, #0x4E           @ Value to select Baud rate in DLL
    STR R1, [R0, #0x00]     @ Write to Divisor Latch Low
    MOV R1, #0x00           @ Value to select divisor
    STR R1, [R0, #0x20]     @ Write to Mode Definition 1 register
    MOV R1, #0x03           @ Value to switch to Operational mode
    STR R1, [R0, #0x0C]     @ Write to Line Control Register
    NOP

@ Enable IRQ interrupt in CPS registers
    MRS R3, CPSR            @ Copy CPSR to R3
    BIC R3, #0x80           @ Clear bit 7
    MSR CPSR_c, R3          @ Write back to CPSR
    NOP
    
@ Debugging talker
@	MOV R3, #0x0D		    @ Load message initial char
@	LDR R5, =0x48024000     @ Address of UART2 transmit buffer
@    STR R3, [R5]            @ Write character to transmit buffer  
@    MOV R3, #0x68			@ "h"
@    STRB R3, [R5]
@    MOV R3,#0x400000		@ Value for countdown (2 secs)
@	WAIT2:
@		NOP
@		SUBS R3, #0x01	    @ Subtract
@		BNE WAIT2			@ If counter not zero, return to wait    
@    NOP
@    MOV R3, #0x69			@ "i"
@    STRB R3, [R5]
@    MOV R3,#0x400000		@ Value for countdown (2 secs)
@	WAIT3:
@		NOP
@		SUBS R3, #0x01	    @ Subtract
@		BNE WAIT3			@ If counter not zero, return to wait    
@    NOP    
@    MOV R3, #0x0D			@ Load message terminating char
@    STRB R3, [R5]			@ Write character to transmit buffer
    
@ Wait for interrupt
    LOOP:   NOP
            B LOOP
            
INT_DIRECTOR: NOP
    STMFD SP!, {R0-R5, LR}  @ Push registers on stack
    @ Check if interrupt from UART2 (INT 74)
    LDR R0, =0x482000D8     @ Address of INTC_PENDING_IRQ2
    LDR R1, [R0]            @ Read INTC_PENDING_IRQ2
    TST R1, #0x400          @ Checking if bit 10 = 1
    BEQ TST_BTTN            @ If interrupt did not come from UART, check button
    LDR R1,=0x48024008		@ Interrupt came from UART, load address for IIR_UART
    LDR R2,[R1]				@ Read IIR_UART register
    MOV R3,#0x00000001      @ Mask for all but bit 0
    AND R2, R3, R2			@ Mask to only read bit 0
    TST R2,#0x0             @ Checking if bit 0 is set (IIT bit 0=interrupt pending)
    BEQ TALKER_SVC          @ Interrupt from UART, go to TALKER_SVC
                            @ Interupt did not come from UART, return to wait loop
	
PASS_ON: NOP    
    @ Turn off NEWIRQA bit in INTC_CONTROL, so processor can respond to new IRQ
    LDR R0,=0x48200048      @ Address of INTC_CONTROL register
    MOV R1, #0x01           @ Value to clear bit 0
    STR R1,[R0]             @ Write to INTC_CONTROL register
    LDMFD SP!, {R0-R5, LR}  @ Restore registers, return address
    SUBS PC, LR, #4         @ Pass execution on to wait loop
    
TST_BTTN: NOP 				@ Testing to see if interupt came from GPIOINT1A
    LDR R0,=0x482000F8      @ Address of INTC_PENDING_IRQ3 register
    LDR R1,[R0]             @ Read INTC_PENDING_IRQ1 register
    TST R1,#0x00000004      @ Test bit 2 (testing if INT 98 signal received)
    LDR R0,=0x4804C02C      @ Load GPIO1_IRQSTATUS_0
    LDR R1, [R0]            @ Read STATUS register
    TSTNE R1,#0x40000000    @ Checking if bit 30 is set
    BEQ PASS_ON             @ Not from button, return to wait loop
    B BUTTON_SVC            @ Interrupt from button, go to BUTTON_SVC
    
BUTTON_SVC: NOP
    LDR R0,=0x4804C02C      @ Address of GPIO1_IRQSTATUS_0 register	
    MOV R1, #0x40000000	    @ Value to turn off GPIO1_30 interrupt request
    STR R1,[R0]             @ Write to GPIO1_IRQSTATUS_0    
    @ Turn off NEWIRQA bit in INTC_CONTROL, so processor can respond to new IRQ
    LDR R0,=0x48200048      @ Address of INTC_CONTROL register
    MOV R1, #0x01           @ Value to clear bit 0 (allowing for new IRQ generation)
    STR R1,[R0]             @ Write to INTC_CONTROL register

    @ Enable UART2 interrupt
    LDR R0,=0x48024004      @ Address of UART2 IER_UART register
    MOV R1, #0xA            @ Value to enable UART2 interrupt for THR and CTS#
    STR R1, [R0]            @ Write to IER_UART register    
    @ Return to wait loop
    LDMFD SP!, {R0-R5, LR}  @ Restore registers, return address
    SUBS PC, LR, #4         @ Return from IRQ interrupt procedure
    
TALKER_SVC: NOP
    @ Check if CTS# is asserted (ready to receive char)
    LDR R0,=0x48024018      @ Address of UART2 Modem Status Register
    LDRB R3,[R0]	        @ Read MSR (resets Modem Status Change Interrupt Bit)
    MOV R2,#0x00000010		@ Mask for all but bit 4
    AND R3, R2, R3			@ Mask MSR to only read bit 4    
    TST R3,#0x10            @ Checking if CTS# is currently asserted (bit 4)    
    BEQ NOCTS               @ CTS# not asserted, go to check THR    
    @ Check if THR is asserted
    LDR R0,=0x48024014      @ Address of Line Status register, LSR      
    LDRB R1,[R0]            @ Read LSR (Does not clear interrupt)
    MOV R2,#0x00000020		@ Mask for everything but bit 5    
    AND R1, R2, R1			@ Mask everything but THR-Ready bit    
    TST R1, #0x20           @ Check if THR-Ready is asserted    
    BEQ GOBCK               @ If not, then exit and wait for THR-Ready  
    B SEND                  @ Else yes, both are asserted, send char
    
NOCTS: NOP
    LDR R0,=0x48024014      @ Address of LSR
    LDRB R1,[R0]            @ Read LSR (Does not clear interrupt)    
    MOV R2, #0x20			@ Mask everything except bit 5
    AND R1, R2, R1			@ Apply mask to R1
    TST R1,#0x20            @ CTS# not asserted, check if THR-Ready is asserted    
    BEQ GOBCK               @ If no, then exit, neither CTS# nor THR are asserted    
    @ Disable interrupt on THR to prevent spinning while waiting for CTS#
    LDR R4,=0x48024004      @ Address of UART interrupt enable register    
    MOV R5,#0x08            @ Disable THR interrupt (mask bit 1)    
    STRB R5,[R4]            @ Write to Interrupt Enable Register    
    B GOBCK                 @ Exit to wait for CTS# interrupt

@ Unmask THR, send char, if end of message reset char count and disable UART interrupt
SEND: NOP
    LDR R4,=0x48024004      @ Address of UART interrupt enable register
    MOV R5,#0x0A            @ Bit 3 = Modem Status bit, Bit 1 = Tx interrupt enable
    STRB R5,[R4]            @ Write to interrupt enable register
    LDR R0,=CHAR_PTR        @ Send char, R0 = address of pointer store
    LDR R1,[R0]             @ R1 = Address of desired char in text string
    LDR R2,=CHAR_COUNT      @ R2 = Address of count store
    LDR R3,[R2]             @ Get current char count value
    LDRB R4,[R1],#1         @ Read char to send from string, incre. ptr in R1
    STR R1,[R0]             @ Put incremented address back in CHAR_PTR location
    LDR R5,=0x48024000      @ Address of UART transmit buffer
    STRB R4,[R5]            @ Write character to transmit buffer
    					    @ clears interrupt source until THR empty again
    SUBS R3, R3, #1         @ Decrement char counter by 1
    STR R3, [R2]		    @ Store new char count    
    BPL GOBCK               @ Greater than or equal to zero = more chars    
    
END_MESSAGE: NOP
    LDR R3,=MESSAGE         @ Done, reload, get address of start of string
    STR R3,[R0]             @ Write in char pointer store location in memory 
    MOV R3,#55     			@ Load original number of chars in string again
    STR R3,[R2]             @ Write back to memory for next message send    						            
	@ Reinit UART and BUTTON
    LDR R0,=0x48024004      @ Address of UART_IER
    MOV R1,#0x00			@ Value to disable UART interrupts
    STR R1,[R0]             @ Write to UART_IER
	LDR R0,=0x48024010 	    @ Address of Modem Control Register
	LDRB R1, [R0]			@ Read value of MCR
	BIC R1, R1, #0x08		@ Clear bit 3 to disable UART interrupts                 
	STRB R1,[R0]			@ Write back to MCR
    @ Turn off NEWIRQA bit in INTC_CONTROL, so processor can respond to new IRQ
    LDR R0,=0x48200048      @ Address of INTC_CONTROL register
    MOV R1, #0x01           @ Value to clear bit 0
    STR R1,[R0]             @ Write to INTC_CONTROL register
    
GOBCK: NOP
    LDMFD SP!, {R0-R5, LR}  @ Restore additional registers, return address
    SUBS PC, LR, #4         @ Return from interrupt to wait loop	
	            
.data

.align 2
STACK1:     .rept 1024
            .word 0x0000
            .endr
STACK2:     .rept 1024
            .word 0x0000
            .endr
            
MESSAGE: .byte 0x0D				

.ascii "Your blood pressure is 120 over 70. Your pulse is 54."

.byte 0x0D

.align 2
MESSAGE_LEN: .word 55		@ Length of MESSAGE (including start and end chars)
CHAR_PTR: .word MESSAGE		@ Pointer to next char to send
CHAR_COUNT: .word 55		@ Counter for number of characters to send
@ Number of chars counts x-1 down to 0
BUTTON_COUNT: .word 0		@ Counter for number of button presses

            
.END